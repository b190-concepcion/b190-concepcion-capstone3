import { Fragment } from "react";

import Banner from "../components/Banner";

export default function Home() {
  const data = {
    title: "Games Meta",
    content: "Super Affordable Play Station Games",
    destination: "/products",
    label: "Buy Now!",
  };
  return (
    <Fragment>
      <Banner data={data} />
    </Fragment>
  );
}
// <Highlights />
