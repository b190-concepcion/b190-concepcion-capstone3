import axios from "../axios";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useStateValue } from "../StateProvider";

function Orders() {
  const [{ user }] = useStateValue();
  const [orders, setOrders] = useState([]);
  console.log(orders);
  useEffect(() => {
    axios
      .post("/orders/myOrders", { email: user.email })
      .then((res) => setOrders(res.data));
  }, []);
  console.log(orders);
  console.log(user);

  console.log(orders);
  return (
    <Main>
      <OrderContainer>
        <h2>Your Orders</h2>

        {orders.map((order) => (
          <OrderDetail>
            <AddressComponent>
              <h4>Shipping Details</h4>

              <div>
                Full Name:
                <p>{order.address.fullName}</p>
                Address:
                <p>{order.address.flat}</p>
                <p>{order.address.area}</p>
                <p>
                  {order.address.city} {order.address.state}
                </p>
                Mobile Number :<p> {order.address.phone}</p>
              </div>
            </AddressComponent>
            <OrderBasket>
              <h4>Order</h4>

              {order.products.map((product) => (
                <Product>
                  <Image>
                    <img src={product.img} alt="" />
                  </Image>
                  <Description>
                    <h4>{product.name}</h4>

                    <p>₱ {product.price}.00</p>
                  </Description>
                </Product>
              ))}
              <h4 className="text-center">
                Subtotal : ₱ <span>{order.price}.00</span>
              </h4>
            </OrderBasket>
          </OrderDetail>
        ))}
      </OrderContainer>
    </Main>
  );
}

const Main = styled.div`
  min-width: 100vw;
  min-height: 100vh;
  display: flex;
  justify-content: center;
`;

const OrderContainer = styled.div`
  background-color: #ebe0d6;
  padding: 15px;
  width: 95%;

  h2 {
    font-weight: 500;
    border-bottom: 1px solid lightgray;
    padding-bottom: 15px;
  }
`;

const OrderDetail = styled.div`
  border-bottom: 1px solid lightgray;
  padding-bottom: 20px;
`;

const AddressComponent = styled.div`
  margin-top: 20px;

  div {
    margin-top: 10px;
    margin-left: 10px;

    p {
      font-size: 14px;
      margin-top: 4px;
    }
  }
`;

const OrderBasket = styled.div`
  margin-top: 20px;

  p {
    font-size: 15px;
    margin-top: 15px;

    span {
      font-weight: 600;
    }
  }
`;

const Product = styled.div`
  display: flex;
  align-items: center;
`;

const Image = styled.div`
  width: 50%;
  border-radius: 20px;
  padding: 0px 0px;
  margin: 0px 0px;
  img {
    width: 25%;
    border-radius: 20px;
  }
`;
const Description = styled.div`
  flex: 0.7;

  h4 {
    font-weight: 600;
    font-size: 18px;

    @media only screen and (max-width: 1200px) {
      font-size: 14px;
    }
  }

  p {
    font-weight: 600;
    margin-top: 10px;
  }

  button {
    background-color: transparent;
    color: #1384b4;
    border: none;
    outline: none;
    margin-top: 10px;
    cursor: pointer;
    &:hover {
      text-decoration: underline;
    }
  }
`;
export default Orders;
