import Banner from "../components/Banner";

export default function Error() {
  const data = {
    title: "404 No Found",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back home",
  };

  return (
    <div>
      <h1>404 Error</h1>
      <h1>Page Not Found</h1>
    </div>
  );
}
