import { Fragment, useEffect, useState, useContext } from "react";
import AdminView from "../components/AdminView";
import UserView from "../components/UserView";
import UserContext from "../UserContext";

export default function Product() {
  const { user } = useContext(UserContext);

  const [product, setProduct] = useState([]);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Fragment>
      {user.isAdmin === true ? (
        <AdminView productData={product} fetchData={fetchData} />
      ) : (
        <UserView productData={product} />
      )}
    </Fragment>
  );
}
