import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { useStateValue } from "../StateProvider";

export default function Address() {
  const [{}, dispatch] = useStateValue();
  const [fullName, setFullName] = useState("");
  const [phone, setPhone] = useState("");
  const [flat, setFlat] = useState("");
  const [area, setArea] = useState("");
  const [landmark, setLandmark] = useState("");
  const [city, setCity] = useState("");
  const [state, setState] = useState("");

  const navigate = useNavigate();
  const deliver = (e) => {
    e.preventDefault();

    dispatch({
      type: "SET_ADDRESS",
      item: {
        fullName,
        phone,
        flat,
        area,
        city,
        state,
      },
    });

    navigate("/payment");
  };

  return (
    <Main>
      <FormContainer>
        <InputContainer>
          <h4 className="text-center pb-2">Shipping Information</h4>
          <p>Full Name</p>
          <input
            onChange={(e) => setFullName(e.target.value)}
            type="text"
            placeholder="Enter your full name"
            value={fullName}
          />
        </InputContainer>
        <InputContainer>
          <p>Mobile Number</p>
          <input
            type="text"
            onChange={(e) => setPhone(e.target.value)}
            value={phone}
          />
        </InputContainer>
        <InputContainer>
          <p>House no. / Street/ Building</p>
          <input
            type="text"
            onChange={(e) => setFlat(e.target.value)}
            value={flat}
          />
        </InputContainer>
        <InputContainer>
          <p>Brgy.</p>
          <input
            type="text"
            onChange={(e) => setArea(e.target.value)}
            value={area}
          />
        </InputContainer>
        <InputContainer>
          <p>City</p>
          <input
            type="text"
            onChange={(e) => setLandmark(e.target.value)}
            value={landmark}
          />
        </InputContainer>
        <InputContainer>
          <p>State/Province</p>
          <input
            type="text"
            onChange={(e) => setCity(e.target.value)}
            value={city}
          />
        </InputContainer>
        <InputContainer>
          <p>Country</p>
          <input
            type="text"
            onChange={(e) => setState(e.target.value)}
            value={state}
          />
        </InputContainer>

        <button onClick={deliver}>Deliver to this Address</button>
      </FormContainer>
    </Main>
  );
}

const Main = styled.div`
  padding: 15px;
`;

const FormContainer = styled.form`
  border: 2px solid #cce7d0;
  box-shadow: 20px 20px 30px rgba(0, 0, 0, 0.1);
  width: 25%;
  min-width: 400px;
  height: fit-content;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 15px;
  background-color: #ebe0d6;
  margin: auto;

  button {
    align-self: flex-center;
    height: 33px;
    width: 250px;
    margin-top: 20px;
    background-color: #ffa32a;
    border: none;
    outline: none;
    border-radius: 5px;
    cursor: pointer;
  }
`;

const InputContainer = styled.div`
  width: 100%;
  padding: 10px;

  p {
    font-size: 14px;
    font-weight: 600;
  }

  input {
    width: 95%;
    height: 33px;
    padding-left: 5px;
    border-radius: 5px;
    border: 1px solid lightgray;
    margin-top: 5px;

    &:hover {
      border: 1px solid orange;
    }
  }
`;
