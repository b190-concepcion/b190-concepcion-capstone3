import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, useEffect } from "react";
import { useStateValue } from "../StateProvider";
import { useNavigate } from "react-router-dom";

import Swal from "sweetalert2";
import { NavItem } from "react-bootstrap";

export default function Logout() {
  const { unsetUser, setUser } = useContext(UserContext);
  const navigate = useNavigate();
  const [{ basket }, dispatch] = useStateValue();
  // clear the local storage
  unsetUser();
  /*
 placing the "setUser" setter function inside of a useEffect is necessary becauase of the updates in React.js that a state of another component cannot be updated whiel trying to render a different component.

 by adding useEffect, this will render Logout oage first before triggering the useEffect which changes the state of the user
*/
  useEffect(() => {
    setUser({ id: null });
    Swal.fire({
      title: "Thank you, Come Again",
      icon: "success",
      text: "See you soon :)",
    });
    dispatch({
      type: "EMPTY_BASKET",
    });
  });
  //Set the user state back to it's original state

  return <Navigate to="/login" />;
}
