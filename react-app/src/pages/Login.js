import { useState, useEffect, useContext, Fragment } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useStateValue } from "../StateProvider";
// User Context
import UserContext from "../UserContext";

// sweetalert2
import Swal from "sweetalert2";

import { StyledForm } from "./StyledForm";

export default function Login() {
  // Allows us to consume the UserContext object and its'sproperties to be used for user validation
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState(false);

  const [{}, dispatch] = useStateValue();

  

  const retrieveUserDetails = (user) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer ${user}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        dispatch({
          type: "SET_USER",
          user: data,
        });
        localStorage.setItem("user", JSON.stringify(data));
        console.log(data);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function loginUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (typeof data.access !== "undefined") {
          console.log(data);
          // JWT will be used to retrieve user information accress the whole frontend application and storing it inside the localStorage
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          // use sweetalert2 (Swal.fire() method) to costumize the alerts sent to the users
          Swal.fire({
            title: "Login Successful!",
            icon: "success",
            text: "Welcome to Games Meta",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed.",
            icon: "error",
            text: "Check your login credentials and try again.",
          });
        }
      });

    setEmail("");
    setPassword("");
  }

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
    <Fragment>
      <StyledForm onSubmit={(e) => loginUser(e)}>
        <h1 className="text-center">Sign-in</h1>
        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            className="logtext"
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            className="logtext"
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button variant="primary" type="submit" id="submitBtn">
            Login
          </Button>
        ) : (
          <Button variant="primary" type="submit" id="submitBtn" disabled>
            Login
          </Button>
        )}
      </StyledForm>
    </Fragment>
  );
}
