import React, { useEffect, useState, useContext } from "react";
import CurrencyFormat from "react-currency-format";
import styled from "styled-components";
import { getBasketTotal } from "../reducer";
import { useStateValue } from "../StateProvider";
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import axios from "../axios";
import { useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

function Payment() {
  const [{ address, basket, user }, dispatch] = useStateValue();
  const [clientSecret, setClientSecret] = useState("");
  const elements = useElements();
  const stripe = useStripe();

  console.log(address);
  console.log(basket);
  console.log(user);

  const navigate = useNavigate();
  useEffect(() => {
    const fetchClientSecret = async () => {
      const data = await axios.post("/payment/create", {
        amount: getBasketTotal(basket),
      });

      setClientSecret(data.data.clientSecret);
    };

    fetchClientSecret();
    console.log("clientSecret is >>>>", clientSecret);
  }, []);

  const confirmPayment = async (e) => {
    e.preventDefault();

    await stripe
      .confirmCardPayment(clientSecret, {
        payment_method: {
          card: elements.getElement(CardElement),
        },
      })
      .then((result) => {
        axios.post("/orders/checkout", {
          basket: basket,
          price: getBasketTotal(basket),
          email: user?.email,
          address: address,
        });

        dispatch({
          type: "EMPTY_BASKET",
        });
        Swal.fire({
          title: "Payment Successfully",
          icon: "success",
          text: "You can check the status of your order!",
        });
        navigate("/");
      })
      .catch((err) => console.warn(err));
  };

  return (
    <Main>
      <ReviewContainer>
        <h2>Review Your Order</h2>

        <AddressContainer>
          <h3>Shipping Details</h3>

          <div>
            Full Name:
            <p>{address.fullName}</p>
            Address:
            <p>{address.flat}</p>
            <p>{address.area}</p>
            <p>
              {address.city} {address.state}
            </p>
            Mobile Number :<p> {address.phone}</p>
          </div>
        </AddressContainer>

        <PaymentContainer>
          <h3>Payment Method</h3>

          <div>
            <h4>Card Details</h4>

            {/* Card Element */}

            <CardElement />
          </div>
        </PaymentContainer>

        <OrderContainer>
          <h3>Your Order</h3>

          <div>
            {basket?.map((product) => (
              <Product>
                <Image>
                  <img src={product.img} alt="" />
                </Image>
                <Description>
                  <h4>{product.name}</h4>

                  <p>₱ {product.price}.00</p>
                </Description>
              </Product>
            ))}
          </div>
        </OrderContainer>
      </ReviewContainer>
      <Subtotal>
        <CurrencyFormat
          renderText={(value) => (
            <>
              <p>
                Subtotal ( {basket.length} items ) : <strong> {value}</strong>
                .00
              </p>
            </>
          )}
          decimalScale={2}
          value={getBasketTotal(basket)}
          displayType="text"
          thousandSeparator={true}
          prefix={"₱"}
        />

        <button onClick={confirmPayment}>Place Order</button>
      </Subtotal>
    </Main>
  );
}
const Main = styled.div`
  padding: 15px;
  display: flex;
  background-color: #ebe0d6;

  @media only screen and (max-width: 1200px) {
    flex-direction: column;
  }
`;

const ReviewContainer = styled.div`
  background-color: #ebe0d6;
  flex: 0.7;
  padding: 15px;
  h2 {
    font-weight: 500;
    border-bottom: 1px solid lightgray;
    padding-bottom: 15px;
  }
`;

const AddressContainer = styled.div`
  margin-top: 20px;
  div {
    margin-top: 10px;
    margin-left: 10px;

    p {
      font-size: 14px;
      margin-top: 4px;
    }
  }
`;

const PaymentContainer = styled.div`
  margin-top: 15px;

  div {
    margin-top: 15px;
    margin-left: 15px;

    p {
      font-size: 14px;
    }
  }
`;

const OrderContainer = styled.div`
  margin-top: 30px;
`;

const Product = styled.div`
  display: flex;
  align-items: center;
`;

const Image = styled.div`
  width: 50%;
  border-radius: 20px;
  padding: 0px 0px;
  margin: 0px 0px;
  img {
    width: 40%;
    border-radius: 20px;
  }
`;
const Description = styled.div`
  flex: 0.7;

  h4 {
    font-weight: 600;
    font-size: 18px;
  }

  p {
    font-weight: 600;
    margin-top: 10px;
  }

  button {
    background-color: transparent;
    color: #1384b4;
    border: none;
    outline: none;
    margin-top: 10px;
    cursor: pointer;
    &:hover {
      text-decoration: underline;
    }
  }
`;
const Subtotal = styled.div`
  margin-top: 100px;
  flex: 0.3;
  background-color: #ebe0d6;
  margin-left: 15px;
  height: 200px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media only screen and (max-width: 1200px) {
    flex: none;
    margin-top: 20px;
  }
  p {
    font-size: 20px;
  }

  small {
    display: flex;
    align-items: center;
    margin-top: 10px;

    span {
      margin-left: 10px;
    }
  }

  button {
    width: 65%;
    height: 33px;
    margin-top: 20px;
    background-color: #ffd814;
    border: none;
    outline: none;

    border-radius: 8px;
  }
`;
export default Payment;
