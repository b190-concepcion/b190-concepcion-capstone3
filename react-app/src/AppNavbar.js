import { useContext, useState } from "react";
import { Fragment } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link, NavLink } from "react-router-dom";
import UserContext from "./UserContext";
import logo from "./images/logo.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartPlus } from "@fortawesome/free-solid-svg-icons";

import Badge from "react-bootstrap/Badge";

import { useNavigate } from "react-router-dom";
import { useStateValue } from "./StateProvider";

export default function AppNavbar() {
  /*
		localStorage.getItem is used  to get a piece of information from the browser's local storage. devs usually insert information in the local storage for specific purposes; one of commonly used purpose is for login in. it accepts 1 argument pertaining to the specific key/property inside the local storage to get its value
	*/
  // uses the state hook to set the initial value for user variable coming from the local storage, specifically the email property
  const [{ basket }, dispatch] = useStateValue();
  const navigate = useNavigate();

  const { user } = useContext(UserContext);

  console.log(user);

  return (
    <section className="header">
      <Navbar.Brand as={Link} to="/">
        <img src={logo}></img>
      </Navbar.Brand>
      <div>
        <ul className="navbar">
          <Navbar expand="lg">
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="navsize">
                <Nav.Link as={NavLink} className="font" to="/" exact>
                  Home
                </Nav.Link>
                <Nav.Link as={NavLink} className="font" to="/products" exact>
                  {" "}
                  Product
                </Nav.Link>
                {user.isAdmin !== true && user.id !== null ? (
                  <Nav.Link as={NavLink} className="font" to="/orders" exact>
                    Orders
                  </Nav.Link>
                ) : (
                  <Fragment></Fragment>
                )}

                {user.id !== null ? (
                  <Nav.Link as={NavLink} className="font" to="/logout" exact>
                    {" "}
                    Logout
                  </Nav.Link>
                ) : (
                  <Fragment>
                    <Nav.Link as={NavLink} className="font" to="/login" exact>
                      {" "}
                      Login
                    </Nav.Link>
                    <Nav.Link
                      as={NavLink}
                      className="font"
                      to="/register"
                      exact
                    >
                      {" "}
                      Register
                    </Nav.Link>
                  </Fragment>
                )}

                {user.isAdmin !== true && user.id !== null ? (
                  <Nav.Link as={NavLink} className="font" to="/checkout" exact>
                    {" "}
                    <FontAwesomeIcon icon={faCartPlus} />
                    <Badge pill bg="danger">
                      {basket?.length}
                    </Badge>
                  </Nav.Link>
                ) : (
                  <Fragment></Fragment>
                )}
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </ul>
      </div>
    </section>
  );
}
