import axios from "axios";

const instance = axios.create({
  baseURL: "https://games-meta.herokuapp.com",
});

export default instance;
