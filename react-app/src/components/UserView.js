import { Fragment, useState, useEffect } from "react";
import ProductCard from "./ProductCard";
import { Row, Col, Card, Button } from "react-bootstrap";

export default function UserView({ productData }) {
  // console.log(coursesData);

  const [product, setProduct] = useState([]);

  useEffect(() => {
    // Map through the courses received from the parent component (courses page) to render the course cards
    const productArr = productData.map((product) => {
      // Returns active courses as "CourseCard" components
      if (product.isActive === true) {
        return (
          <Col xs={12} md={3}>
            <ProductCard productProp={product} key={product._id} />
          </Col>
        );
      } else {
        return null;
      }
    });

    // Set the "courses" state with the course card components returned by the map method
    // Allows the course card components to be rendered in this "UserView" component via the return statement below
    setProduct(productArr);
  }, [productData]);

  return (
    <Fragment>
      <section className="text-center">
        <br></br>
        <h1>PlayStation Games</h1>
        <p>Check-out our biggest release for this month!</p>
        <Row className="product1">{product}</Row>
      </section>
    </Fragment>
  );
}
