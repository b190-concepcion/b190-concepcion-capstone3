// react permits the use of object literals if the dev will import multiple components from the same dependency
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Banner({ data }) {
  const { destination, label } = data;
  return (
    <section className="hero">
      <Row>
        <Col>
          <h4>Our Offers</h4>
          <h2>Super Affordable</h2>
          <h1>PlayStation Games</h1>
          <p>Check our latest games!</p>
          <Link class="buy" to={destination}>
            {label}
          </Link>
        </Col>
      </Row>
    </section>
  );
}
