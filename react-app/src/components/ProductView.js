import { useState, useEffect, useContext } from "react";
import { Card, Button } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import UserContext from "../UserContext";
import { useStateValue } from "../StateProvider";

import styled from "styled-components";

export default function CourseView() {
  const { user } = useContext(UserContext);

  const { productId } = useParams();
  const [product, setProduct] = useState({});

  const [name, setName] = useState("");
  const [platform, setPlatform] = useState("");
  const [img, setImg] = useState("");
  const [stocks, setStocks] = useState(0);
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  const Image = styled.img`
    width: 100%;
    border-radius: 20px;
    padding: 0px 0px;
    margin: 0px 0px;
  `;

  useEffect(() => {
    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setPlatform(data.platform);
        setDescription(data.description);
        setStocks(data.stocks);
        setImg(data.img);
        setPrice(data.price);
        setProduct(data);
      });
  }, [productId]);

  const [{ basket }, dispatch] = useStateValue();
  console.log("basket >>>>", basket);
  const addToBasket = (e) => {
    e.preventDefault();

    dispatch({
      type: "ADD_TO_BASKET",
      item: {
        name,
        img,
        price,
        img,
      },
    });
  };

  return (
    <Card className="prod1">
      <Card.Body className="text-center">
        <Image src={img} />

        <Card.Text className="plat">{platform}</Card.Text>
        <Card.Title className="size1">{name}</Card.Title>

        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>

        <Card.Text className="size2">₱ {price}.00</Card.Text>

        {user.id !== null ? (
          <Button variant="primary" onClick={addToBasket} block>
            Add to Cart
          </Button>
        ) : (
          <Link className="btn btn-danger btn-block" to="/login">
            Login to Buy
          </Link>
        )}
      </Card.Body>
    </Card>
  );
}
