import { Card, Button } from "react-bootstrap";
import { useContext } from "react";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

import styled from "styled-components";

import { useStateValue } from "../StateProvider";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCartShopping,
  faInfoCircle,
} from "@fortawesome/free-solid-svg-icons";

export default function ProductCard({ productProp }) {
  const { name, platform, img, description, price, _id } = productProp;

  const { user } = useContext(UserContext);

  const Image = styled.img`
    width: 100%;
    border-radius: 20px;
    padding: 0px 0px;
    margin: 0px 0px;
  `;
  const [{ basket }, dispatch] = useStateValue();
  console.log("basket >>>>", basket);
  const addToBasket = (e) => {
    e.preventDefault();

    dispatch({
      type: "ADD_TO_BASKET",
      item: {
        name,
        img,
        price,
        _id,
      },
    });
  };

  return (
    <Card className="product4">
      <Card.Body>
        <Link to={`/products/${_id}`}>
          <Image src={img} />
        </Link>
      </Card.Body>
      <div className="des">
        <Card.Text className="plat">{platform}</Card.Text>
        <Card.Title className="size1">{name}</Card.Title>

        <Card.Text className="size2">₱ {price}.00</Card.Text>
      </div>
      <Link to={`/products/${_id}`}>
        <FontAwesomeIcon className="cart2" icon={faInfoCircle} />
      </Link>
      {user.id !== null ? (
        <Button className="set1" onClick={addToBasket}>
          <FontAwesomeIcon className="cart" icon={faCartShopping} />
        </Button>
      ) : (
        <Button className="set1" onClick={addToBasket} disabled>
          <FontAwesomeIcon className="cart" icon={faCartShopping} />
        </Button>
      )}
    </Card>
  );
}
