import "./App.css";
// when the dev wants to render multiple components inside a reuseable function such as App, Fragment component of react is used to enclosed these components to avoid errors in our frontend application; the pair of <> and </> can also be used
import { UserProvider } from "./UserContext";

import { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";

// Web Components
import AppNavbar from "./AppNavbar";

// Web Pages
import Home from "./pages/Home";
import Product from "./pages/Product";
import ProductView from "./components/ProductView";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import Address from "./pages/Address";
import Orders from "./pages/Orders";
import Payment from "./pages/Payment";
import Checkout from "./pages/Checkout";

const promise = loadStripe(
  "pk_test_51LUOpGDXsN77AeTto7OzTg7nJIZUSGyNTbGzc0iyJdfQ5P0yKsmBXChu8EMRfexxSOcMTUfbjrOO5lWaZfYNGqpZ00ly9svfWm"
);

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    // console.log(user);

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Set the user states values with the user details upon successful login.
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });

          // Else set the user states to the initial values
        } else {
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />

        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/products" element={<Product />} />
          <Route path="/products/:productId" element={<ProductView />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="*" element={<Error />} />
          <Route path="/address" element={<Address />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route
            path="/payment"
            element={
              <Elements stripe={promise}>
                <Payment />
              </Elements>
            }
          />
          <Route path="/orders" element={<Orders />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
