const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");

//Create orders

/*
module.exports.getUserOrder = (data, requestBody) => {
  return Order.find({})
    .populate("products.userId", "-email -password -__v -isAdmin")
    .populate(
      "products.productId",
      "-isActive  -createdOn -__v -description -stocks"
    )
    .then((result) => {
      return result;
    });
};
*/
//Retrieve all orders
module.exports.getAllOrders = () => {
  return Order.find({})
    .populate("products.userId", "-email -password -__v -isAdmin")
    .populate(
      "products.productId",
      "-isActive  -createdOn -__v -description -stocks"
    )
    .then((result) => {
      console.log(result);
      return result;
    });
};
