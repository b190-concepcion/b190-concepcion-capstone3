const express = require("express");
const mongoose = require("mongoose");

// routes
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const Order = require("./models/Order.js");

const stripe = require("stripe")(
  "sk_test_51LUOpGDXsN77AeTtESPwQNUOUObLSAPWLY33qZafbn7eljrTRhvRhlkVmQZzRQzhUXzfh6DOIOfMsiUC5y1t1keC00sM88ZVgy"
);

// Allows us to control the app's Cross-Origin Resource Sharing settings
const cors = require("cors");

const app = express();

// MongoDB Connection
mongoose.connect(
  "mongodb+srv://aronsc26:diwa1234@wdc028-course-booking.sysnpgd.mongodb.net/b190-concepcion-ecommerce?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));

//allows all resources to access our backend application
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

// API for PAYMENT

app.post("/payment/create", async (req, res) => {
  const total = req.body.amount;
  console.log("Payment Request received for this PHP:", total);

  const payment = await stripe.paymentIntents.create({
    amount: total * 100,
    currency: "php",
  });

  res.status(201).send({
    clientSecret: payment.client_secret,
  });
});

app.post("/orders/MyOrders", (req, res) => {
  const email = req.body.email;

  Order.find((err, result) => {
    if (err) {
      console.log(err);
    } else {
      const userOrders = result.filter((order) => order.email === email);
      res.send(userOrders);
    }
  });
});

app.post("/orders/checkout", (req, res) => {
  const products = req.body.basket;
  const price = req.body.price;
  const email = req.body.email;
  const address = req.body.address;

  const orderDetail = {
    products: products,
    price: price,
    address: address,
    email: email,
  };

  Order.create(orderDetail, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      return true;
    }
  });
});

/*
	Heroku deployment
		Procfile - needed file in heroku to determine the command that will be used when starting the server (web: node app)
*/

// process.env.PORT handles the environment of the hosting websites should app be hosted in a website such as Heroku
app.listen(process.env.PORT || 4000, () => {
  console.log(`API now online at port ${process.env.PORT || 4000}`);
});
