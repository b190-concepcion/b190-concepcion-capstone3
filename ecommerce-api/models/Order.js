const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  price: Number,
  products: Array,
  email: String,
  address: Object,
  purchasedOn: {
    type: Date,
    // creates a new "date" that stores the current date and time of the course creation
    default: new Date(),
  },
});

module.exports = mongoose.model("Order", orderSchema);
