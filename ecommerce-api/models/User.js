const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({
	firstName:{
		type: String,
		//requires the data for this field to be uncluded when creating a document/record
		required: [true, "First Name is required"]
	},
	lastName:{
		type: String,
		required: [true,"Last Name is required"]
	},
	email:{
		type: String,
		required: [true,"Email is required"]
	},
	password:{
		type: String,
		required: [true,"Password is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	mobileNo:{
		type: String,
		required: [true,"Mobile Number is required"]
	}
});

module.exports = mongoose.model("User",userSchema);